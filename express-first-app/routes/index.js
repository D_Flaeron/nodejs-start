var express = require('express');
var router = express.Router();
var url = 'mongodb://mongodatabaseUser:UserPassword^_^@ds149479.mlab.com:49479/mongodatabase';
var mongoose = require('mongoose');
mongoose.connect(url);

const Schema = mongoose.Schema;

const userDataSchema = new Schema({
    title: {type: String, required: true},
    content: String,
    author: String
}, {collection: 'user-data'});

const UserData = mongoose.model('UserData', userDataSchema);

/* GET home page. */
router.get('/', (req, res, next) => {
    res.render('index');
});

router.get('/get-data', (req, res, next) => {
    UserData.find()
        .then((doc) => {
            res.send(doc)
        })
});

router.post('/insert', (req, res, next) => {
    let item = {
        title: req.body.title,
        content: req.body.content,
        author: req.body.author
    };

    let data = new UserData(item);
    data.save((err) => {
        if (err) {
            res.send({status: 'ERROR'});
        }
        else {
            res.send({status: 'OK'});
        }
    });
});

router.post('/update', (req, res, next) => {
    let item = {
        title: req.body.title,
        content: req.body.content,
        author: req.body.author
    };
    let id = req.body.id;
    UserData.findById(id, (err, doc) => {
        if (err) {
            res.send({status: 'ERROR'});
        }
        else {
            res.send({status: 'OK'});
        }
        doc.title = req.body.title;
        doc.content = req.body.content;
        doc.author = req.body.author;
        doc.save();
    });
});

router.post('/delete', (req, res, next) => {
    let id = req.body.id;
    UserData.findByIdAndRemove(id).exec()
        .then((doc) => {
            res.send({status: 'OK'});
        }).catch((error) => {
        res.send({status: 'ERROR'});
    });
});


module.exports = router;